<?php

$org="Plus, spending too much time writing makes a project drag on – and you risk losing your mojo and enthusiasm.

How long should it take to write a 500-word article?.";

function limit_words($string, $word_limit)
{
    $words = explode(" ",$string);
    return implode(" ", array_splice($words, 0, $word_limit));
}

echo limit_words($org,200);

?>